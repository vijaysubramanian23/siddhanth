import React, { Component } from 'react';
//import logo from './logo.svg';
//import './App.css';
import List from './List';

class App extends Component {
  constructor(){
    super();
    this.state = {
      fruits: [],
      vegetables: [],
      plants: []
    }
  }
  componentDidMount() {
    //api call 1
    this.setState({
      fruits: [
        {item: "Apple", color: "red"},
        {item: "Mango", color: "yellow"},
        {item: "banana", color: ""}
      ],
      vegetables: [
        {item: "Beetroot", color: "red"},
        {item: "carrot", color: "orange"},
        {item: "tomato", color: ""}
      ]
    })

    // //api call 2
    // this.setState({
    //   vegetables: [
    //       {item: "Beetroot", color: "red"},
    //       {item: "carrot", color: "orange"},
    //       {item: "tomato", color: ""}
    //   ]
    // })

    //api call 3
    this.setState({
      plants: [
          {item: "Hibiscus", color: "red"},
          {item: "Mango tree", color: "brown"},
          {item: "jack tree", color: ""}
      ]
    })

  }
  render() {
    console.log(this.state);
    return (
      <div className="App">
          <p>Fruits List</p>
          <List items= { this.state.fruits} />

          <p>Vegetables List </p>
          <List items= { this.state.vegetables }/>

          <p>Plants List </p>
          <List items= { this.state.plants }/>
      </div>

      // <div className="App">
      //   <header className="App-header">
      //     <img src={logo} className="App-logo" alt="logo" />
      //     <h1 className="App-title">Welcome Vijay</h1>
      //   </header>
      //   <p className="App-intro">
      //     Hi!<br/>
      //     To get started, edit <code>src/App.js</code> and save to reload.
      //   </p>
      // </div>
    );
  }
}

export default App;
