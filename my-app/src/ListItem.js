import React , {Component} from 'react';

class ListItem extends Component {
    // constructor(){
    //     super();
    // }
    render() {
        return (
            <li>
                <p>Name: {this.props.item} </p>
                 {this.props.color &&  <p>color: {this.props.color} </p>}
            </li>
        );
    }
}
export default ListItem;