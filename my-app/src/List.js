import React, { Component} from 'react';
import ListItem from './ListItem';

class List extends Component {
    constructor(){
        super();
        
        this.state = {
            fruits: []
        }
    }

    componentDidMount() {
        //after some api call
        
    }
    render() {
        // whenever state or props sent to the component ,render is called
        console.log(this.state);
        return (
            <ul>
                {
                    this.props.items.map( (item) =>{
                        return (
                        <ListItem item = {item.item} color = {item.color}/>
                        )
                    })
                }
            </ul>
        );
    }
}
export default List;