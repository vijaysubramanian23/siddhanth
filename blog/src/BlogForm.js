import React, { Component} from 'react';

class Form extends Component {
    constructor(){
        super();
        this.state = {
            title:'',
            content:''
        }
        this.onChange = this.onChange.bind(this);
        this.formSubmit = this.formSubmit.bind(this);
    }
    formSubmit(e){
        e.preventDefault();
        console.log(this.state.title);
        console.log(this.state.content);
        this.setState({
            title:"",
            content:""
        })
    }
    onChange(e){
        this.setState({[e.target.name] : e.target.value})
    }
    
    render(){
        return(
            <div>
                <form onSubmit={this.formSubmit}>
                    Title:<br/>
                    <input type="text" name="title" value={this.state.title} onChange = {this.onChange}/><br/><br/>
                    Content:<br/>
                    <textarea name="content" value={this.state.content} onChange = {this.onChange}/><br/><br/>
                    <input type="submit" value="Submit" />
                </form>
            </div>
        );
        
    }
    
}
export default Form;