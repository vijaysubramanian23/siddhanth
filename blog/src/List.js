import React, {Component} from 'react';
import { connect } from 'react-redux';
import { fetchPosts } from './actions';

class List extends Component {
    constructor(){
        super();
        this.state = {
            posts: []
        }
    }

    componentDidMount(){
        // fetch('https://jsonplaceholder.typicode.com/posts/')
        // .then(response => response.json())
        // .then(result => {
        //     this.setState({posts : result})
        //     // json.map((post) => {
        //     //     this.posts.title = post.title
        //     //     this.posts.content = post.body
        //     // })
        //     console.log(this.state.posts);
        // })
        // //const title 
        this.props.fetchPosts();
    }

    render(){
        console.log(this.props.posts);
        const { posts } = this.props;
        if(this.props.posts && this.props.posts.posts && this.props.posts.posts.length > 0){ 
            return (
                <ul>
                    {
                        posts.posts.map(function(post) {
                            return (
                                <li>
                                <p>{ post.title }</p>
                                <p>{ post.body }</p> 
                                </li>
                            );
                        })
                    }
                </ul>
            );
        } else {
            return (
                <h3>Loading...</h3>
            );
        }
    }
}

const mapStateToProps = (state) => {
    return{
        posts: state.posts
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        fetchPosts : () => dispatch(fetchPosts())
    };
};
export default connect(mapStateToProps,mapDispatchToProps)(List);