import React, { Component } from 'react';
import Form from './BlogForm'
import List from './List'

import store from './store';
import { Provider } from 'react-redux';

class App extends Component {
  
  render() {
    return (
      <Provider store= { store }>
        <div className="App">
          <h1>BLOG</h1>
          <Form/><br/>
          <List/>
        </div>
      </Provider>
    );
  }
}

export default App;
